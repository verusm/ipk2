/*                                                                            
*   Nazev: client.c                                                    
*   Autor: Vera Mullerova, xmulle17@stud.fit.vutbr.cz                                                                 
*   Datum: 12.4.2011                                                          
*   Popis: Klient pro implementace vypisu obsahu adresare vzdaleneho souboroveho systemu.             
*/

// vlozeni knihovny, ve ktere jsou definovane vsechny pouzivane funkce a k nim potrebne knihovny
#include "client.h"

/** Hlavni program */  
int main(int argc, char **argv){
  string param1;
  string adresar;
  // kontrola a zpracovani povinnych parametru a jejich poctu
  if (argc < 2){
    fprintf(stderr, "Chybi povinny parametr jmeno_serveru:port!\n");
    return EXIT_FAILURE;
  }
  else if (argc < 3){
    fprintf(stderr, "Chybi povinny parametr absolutni cesta k adresari!\n");
    return EXIT_FAILURE;
  }
  else if (argc != 3){
    fprintf(stderr, "Je zadano vice parametru, nez se ocekava!\n");
    return EXIT_FAILURE;
  }
  else {
    param1 = argv[1];	// do promenne param1 se zapise jmeno_serveru:port
    adresar = argv[2];	// do promenne adresar se zapise absolutni cesta k adresari
  }

  int port;
  string server;
  
  int status = ControlName(&port, &server, param1);
  if (status != 0){
    return EXIT_FAILURE;
  }
  
  // nahrazeni pozadovanych znaku v adresari
  string c;	// hledany znak
  string ch;	// nahrazujici znak 
  c = "%"; ch = "%25";
  ChangeAdr(c, ch, &adresar);
  c = " "; ch = "%20";
  ChangeAdr(c, ch, &adresar);

  if ((Connection(server, port, adresar)) != 0){
    return EXIT_FAILURE;
  }  
  
  return EXIT_SUCCESS;
} // konec funkce main()

/**
 * Funkce pro zamenu nepodporovanych znaku (mezera a %) v nazvu adresare.
 * @param c Znak (rezetec), ktery ma byt nahrazen.
 * @param ch Rezetec, kterym ma byt pozadovany znak nahrazen.
 * @param *adr Nazev adresare, ve kterem se nahrazuje.
 */
void ChangeAdr(string c, string ch, string *adr){
  string nadr = *adr;  
  size_t found = 0;
  while (found != string::npos){
    found = nadr.find(c, found+1);
    if (found != string::npos)
      nadr.replace(found,c.length(),ch);    
  }
  *adr = nadr;
}

/** 
 * Funkce pro kontrolu a zpracovani jmena serveru a cisla portu.
 * @param *port Cislo portu.
 * @param *server Jmeno serveru.
 * @param param1 1. parametr, ze ktere jsou ziskany hodnoty jmeno serveru a cislo portu.
 * @return Vraci pripadnou chybu. 
 */
int ControlName(int *port, string *server, string param1){
  regmatch_t pmatch[CPARAM];

  string pattern = "^([^:/?]+)(:([0-9]+){1})$";	// regularni vyraz pro kontrolu jmena serveru a cisla portu
  
  regex_t re;
  int status;
  
  if(regcomp( &re, pattern.c_str(), REG_EXTENDED)!= 0) {	// regcomp => prevod do tvaru pro regexec
    fprintf(stderr, "Error pri kompilaci regexp!\n");
    return -1;
  }
    
  status = regexec(&re, param1.c_str(), 5, pmatch, 0);

  // rozdeleni (rozparserovani) 1.parametru na jednotlive potrebne casti
  if(status == 0){
    *server = param1.substr(pmatch[1].rm_so, pmatch[1].rm_eo-pmatch[1].rm_so);
    if (pmatch[3].rm_so > 0){
      *port = atoi((param1.substr(pmatch[3].rm_so, pmatch[3].rm_eo-pmatch[3].rm_so)).c_str());
    }
  }
  else {
    fprintf(stderr, "Spatny nazev serveru nebo cislo portu!\n");
    return status;
  }
  regfree( &re);  
  return status;
}

/**
 * Funkce pro zajisteni komunikace se serverem.
 * @param server Jmeno serveru.
 * @param port Cislo portu, na kterem se komunikuje.
 * @param adresar Jmeno adresare.
 * @return Vraci pripadnou chybu.
 */
int Connection(string server, int port, string adresar){
  int s, n;	// pomocne promenne  
  string msg = "7000 " + adresar + "\r\n";	// zprava musi byt vzdy ukoncena \r\n

  struct sockaddr_in sin;	 // struktura obsahujici port a jmeno serveru
  struct hostent *hptr;		 // struktura obsahujici "entity" hosta
  
  if ((s = socket(PF_INET, SOCK_STREAM, 0 )) < 0) {	// vytvoreni soketu
    fprintf(stderr, "Chyba pri vytvareni soketu!\n");
    return -1;
  }

  sin.sin_family = PF_INET;	// nastaveni rodiny protokolu
  sin.sin_port = htons(port);	// nastaveni cisla portu
  
  if ((hptr =  gethostbyname(server.c_str())) == NULL){
    fprintf(stderr, "Spatne jmeno serveru: %s \n", server.c_str());
    return -1;
  }
   
  memcpy( &sin.sin_addr, hptr->h_addr, hptr->h_length); 
  
  if (connect (s, (struct sockaddr *)&sin, sizeof(sin) ) < 0){		// navazani spojeni
    fprintf(stderr, "Chyba pri navazovani spojeni!\n");
    return -1;
  }
  
  if (write (s, msg.c_str(), strlen(msg.c_str()) +1) < 0) { 	// odesilani zpravy serveru
    fprintf(stderr, "Chyba pri odesilani zpravy serveru!\n");
    return -1;
  } 
  
  int stat = 1;
  char message[2];
  msg.clear();	// vymazani retezce msg

  // cyklus pro cteni odpovedi od serveru
  while(stat){
    if (( n = read(s, message, 1)) < 0) {	// cteni odpovedi
      fprintf(stderr, "Chyba pri cteni zpravy od severu1!\n");
      return -1;
    }    
    /* overeni konce prijate zpravy \n\n */
    if (message[0] == '\r'){
      msg = msg + message[0];
      if (( n = read(s, message, 1)) < 0) {	// cteni odpovedi
	fprintf(stderr, "Chyba pri cteni zpravy od severu2!\n");
	return -1;
      }
      if (message[0] == '\n'){
	msg = msg + message[0];
	stat = 0;
      }
      else
	msg = msg + message[0];
    }
    else
      msg = msg + message[0];
  } //konec while
  
  // parserovani zpravy od serveru
  if (parseMessage(&msg) != 0){
    return -1;
  }
  
  if (close(s) < 0) { 	// ukonceni spojeni
    fprintf(stderr, "Chyba pri ukoncovani spojeni!");
    return -1;
  }  
  return 0;
}

/**
 * Funkce pro parserovani zpravy od serveru.
 * @param *message Zprava od serveru.
 * @return Vraci pripadnou chybu.
 */
int parseMessage(string *message){
  string msg = *message;
  
  int status;
  int kod;
  
  regmatch_t pmatch[CPARAM];
  string pattern = "^(8[0-9]{3}) (.+)\r\n$";	// regularni vyraz pro kontrolu zpravy od serveru
  
  regex_t re; 
  if(regcomp( &re, pattern.c_str(), REG_EXTENDED)!= 0) {	// regcomp => prevod do tvaru pro regexec
    fprintf(stderr, "Error pri kompilaci regexp!\n");
    return -1;
  }
    
  status = regexec(&re, msg.c_str(), 5, pmatch, 0);;
  
  // nalezeni zaslaneho kodu a zpravy od severu
  if(status == 0){
    kod = atoi(msg.substr(pmatch[1].rm_so, pmatch[1].rm_eo-pmatch[1].rm_so).c_str());
    msg = msg.substr(pmatch[2].rm_so, pmatch[2].rm_eo-pmatch[2].rm_so);
  }
  else {
    fprintf(stderr, "Server nedodrzel protokol pro komunikaci!\n");
    return -1;
  }
  regfree( &re);
  
  switch(kod){
    case 8001: break;
    case 8101:
    case 8102:
    case 8110:
    case 8111:
    case 8112:
    case 8113:
    case 8114:
    case 8115:
    case 8116:
    case 8117:
    case 8118:
    case 8119:
    case 8199: fprintf(stderr, "%d ", kod); break;
    default: fprintf(stderr, "Chybny kod od serveru!\n"); return -1;
  }

  fprintf(stdout, "%s", msg.c_str());
  
  *message = msg;
  return 0;
}
