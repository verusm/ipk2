/*                                                                            
*   Nazev: client.h                                                    
*   Autor: Vera Mullerova, xmulle17@stud.fit.vutbr.cz                                                                 
*   Datum: 12.4.2011                                                          
*   Popis: Hlavickovy souboru pro client.c.             
*/

#ifndef _CLIENT_H_
#define _CLIENT_H_

#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <ctype.h>
#include <string.h>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netdb.h>
#include <regex.h>
#include <locale.h>

#include <iostream>

#define CPARAM 5

using namespace std;

/** Deklarace pouzivanych funkci. */
int ControlName(int *port, string *server, string url);
int Connection(string server, int port, string adresar);
int parseMessage(string *message);
void ChangeAdr(string c, string ch, string *adr);

#endif
