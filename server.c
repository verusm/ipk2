/*                                                                            
*   Nazev: server.c                                                    
*   Autor: Vera Mullerova, xmulle17@stud.fit.vutbr.cz                                                                 
*   Datum: 12.4.2011                                                          
*   Popis: Konkurentni server pro implementace vypisu obsahu adresare vzdaleneho souboroveho systemu.             
*/

// vlozeni knihovny, ve ktere jsou definovane vsechny pouzivane funkce a k nim potrebne knihovny
#include "server.h"

/** Hlavni program */  
int main(int argc, char **argv){
  int port;
  string param1;
  
  // kontrola a zpracovani povinneho parametru
  if (argc < 3){
    fprintf(stderr, "Chybi povinny parametr cislo portu!\n");
    return EXIT_FAILURE;
  }
  else if (argc != 3){
    fprintf(stderr, "Je zadano vice parametru, nez se ocekava!\n");
    return EXIT_FAILURE;
  }
  else {
    if (strcmp(argv[1],"-p") != 0){
      fprintf(stderr, "Chybny parametr!\n");
      return EXIT_FAILURE;
    }
    port = atoi(argv[2]);	// do promenne port se zapise cislo portu
  }

  // komunikace s klienty
  if ((Connection(port)) != 0){
    return EXIT_FAILURE;
  }   
  return EXIT_SUCCESS;
} // konec funkce main()

/**
 * Funkce pro zajisteni komunikace s klienty.
 * @param port Cislo portu,na kterem se komunikuje.
 * @return Vraci pripadnou chybu.
 */
int Connection(int port){
  string msg;
  int s_sockfd, c_sockfd;
  int s_len;
  socklen_t c_len;
  struct sockaddr_in s_address;
  struct sockaddr_in c_address;
  
  // vytvoreni soketu
  if ((s_sockfd = socket(PF_INET, SOCK_STREAM, 0)) < 0){
    fprintf(stderr, "Chyba pri vytvareni soketu!\n");
    return -1;
  }
  
  s_address.sin_family = PF_INET;
  s_address.sin_addr.s_addr = htonl(INADDR_ANY);
  s_address.sin_port = htons(port);
  
  s_len = sizeof(s_address);
  
  if (bind(s_sockfd, (struct sockaddr *)&s_address, s_len) < 0){
    fprintf(stderr, "Chyba pri provadeni bind!\n");
    return -1;
  }
  
  if (listen(s_sockfd, 5)){
    fprintf(stderr, "Chyba pri provadeni listen!\n");
    return -1;
  }  
  // potlaceni signalu
  signal(SIGCHLD, SIG_IGN);
  
  while(1){
    // prijem spojeni
    c_len = sizeof(c_address);
    if ((c_sockfd = accept(s_sockfd, (struct sockaddr *)&c_address, &c_len)) < 0 ) {
      fprintf(stderr,"Chyba pri prijimani spojeni!\n");  // accept error
      return -1;
    }
    //rozvetvenim se vytvori samostatny proces pro tohoto klienta
    if (fork() == 0){
      // jsme v potomku - muzeme zacit cist a zapisovat      
      int stat = 1;
      char message[2];
      // cyklus pro cteni zpravy od klienta
      while(stat){
	if ((read(c_sockfd, message, 1)) < 0) {	// cteni zpravy
	  fprintf(stderr, "Chyba pri cteni zpravy od klienta!\n");
	  return -1;
	}    
	// overeni konce prijate zpravy \r\n 	
	if (message[0] == '\r'){
	  msg = msg + message[0];
	  if ((read(c_sockfd, message, 1)) < 0) {	// cteni odpovedi
	    fprintf(stderr, "Chyba pri cteni zpravy od klienta!\n");
	    return -1;
	  }
	  if (message[0] == '\n'){
	    msg = msg + message[0];
	    stat = 0;
	  }
	  else
	    msg = msg + message[0];
	}
	else
	  msg = msg + message[0];
      } //konec while - cteni zpravy
      
      string adresar;
      // rozdeleni zpravy
      int status = parseMsg(&adresar, &msg);
      if (status != 0){
	if (status == 1){	// pokud je vracena hodnota 1, nastala chyba kvuli nedodrzeni aplikacniho protokolu
	  if ((write(c_sockfd, msg.c_str(), strlen(msg.c_str()))) < 0) {
	    fprintf(stderr, "Chyba pri zasilani zpravy klientovi!\n");
	    return -1;
	  }
	  return -1;
	}
	else {
	  return -1;
	}
      }

      msg.clear();
      if (printDir(adresar, &msg) != 0){	// vypsani pozadovaneho adresare do stringu msg, ktery se nasledne odesle klientovi
	if ((write(c_sockfd, msg.c_str(), strlen(msg.c_str()))) < 0) {
	  fprintf(stderr, "Chyba pri zasilani zpravy klientovi!\n");
	  return -1;
	}
	return -1;
      }
      msg = msg + "\r\n";	// ukonceni zasilane zpravy - dodrzeni aplikacniho protokolu 

      if ((write(c_sockfd, msg.c_str(), strlen(msg.c_str()))) < 0) {
	fprintf(stderr, "Chyba pri zasilani zpravy klientovi!\n");
	return -1;
      }
      return 0;      
    } // konec if(fork() == 0)
    else {
      if (close(c_sockfd) < 0) {
	fprintf(stderr, "Chyba pri ukoncovani spojeni!\n");
	return -1;
      }
    }    
  } //konec while(1)
  
  if (close(s_sockfd) < 0) {
    fprintf(stderr, "Chyba pri ukoncovani spojeni!\n");
    return -1;
  }
  return 0;
}

/**
 * Funkce pro zamenu nepodporovanych znaku (mezera a %) v nazvu adresare.
 * @param c Znak (rezetec), ktery ma byt nahrazen.
 * @param ch Rezetec, kterym ma byt pozadovany znak nahrazen.
 * @param *adr Nazev adresare, ve kterem se nahrazuje.
 */
void ChangeAdr(string c, string ch, string *adr){
  string nadr = *adr;  
  size_t found = 0;
  while (found != string::npos){
    found = nadr.find(c, found+1);
    if (found != string::npos)
      nadr.replace(found,c.length(),ch);    
  }
  *adr = nadr;
}

/**
 * Funkce pro vypsani pozadovaneho adresare.
 * @param adresar Adresar, ktery bude vypsan.
 * @param *message Zprava, do ktere se zapise vypis adresare.
 * @return Vraci pripadnou chybu.
 */
int printDir(string adresar, string *message){
  DIR *dp;
  struct dirent *d;
  string msg = *message;
  msg.clear();
  
  ChangeAdr("%20", " ", &adresar);
  ChangeAdr("%25", "%", &adresar);

  // otevreni pristupu k adresari
  if ((dp = opendir(adresar.data())) == NULL){    
    switch(errno){	// pripadne navraceni chyby
      case EACCES: msg = "8110 Permission denied.\n\r\n"; break;
      case ELOOP: msg = "8111 Too many levels of symbolic links.\n\r\n"; break;
      case ENAMETOOLONG: msg = "8112 Filename too long.\n\r\n"; break;
      case ENOENT: msg = "8113 No such file or directory.\n\r\n"; break;
      case ENOTDIR: msg = "8114 Not a directory.\n\r\n"; break;
      case EMFILE: msg = "8115 Too many open files.\n\r\n"; break;
      case ENFILE: msg = "8116 Too many files open in system.\n\r\n"; break;   
      default: msg = "8199 Neznama chyba.\n\r\n"; break;
    }
    *message = msg;
    return -1;
  }
  
  chdir(adresar.data());	// prejdeme do zadaneho adresare
  
  msg = "8001 ";
  
  //cteni v adresari
  while((d = readdir(dp)) != NULL){
    if (DT_DIR == d->d_type){	// jde o adresar
      if(strcmp(".", d->d_name) == 0 || strcmp("..", d->d_name) == 0)
	continue;
      msg = msg + d->d_name + "\n";
    }
    else if (DT_REG == d->d_type){	// jde o obycejny soubor
      msg = msg + d->d_name + "\n";     
    }
    else if (DT_LNK == d->d_type){	// jde o symbolicky link
      msg = msg + d->d_name + "\n";
    }
    else {}
  } // konec while  
  
  // zavreni pristupu k adresari
  if (closedir(dp) != 0){
    switch(errno){	// pripadne vypsani chyby
      case EBADF: msg = "8118 Bad file descriptor.\n\r\n"; break;
      case EINTR: msg = "8119 Interrupted function.\n\r\n"; break;
      default: break;
    }
    *message = msg;
    return -1;
  }

  *message = msg;
  return 0;
}

/**
 * Funkce pro parserovani zpravy od klienta.
 * @param *adresar Do teto promenne se nahraje adresar, u ktereho je pozadovan vypis.
 * @param *message Zprava od klienta.
 * @return Vraci pripadnou chybu.
 */
int parseMsg(string *adresar, string *message){
  string msg = *message;
  string adr = *adresar;
  
  int status;
  int kod;
  
  regmatch_t pmatch[CPARAM];
  string pattern = "^(7[0-9]{3}) ([^\r\n]+)\r\n$";		// regularni vyraz pro kontrolu zpravy od klienta
  
  regex_t re; 
  if(regcomp( &re, pattern.c_str(), REG_EXTENDED)!= 0) {	// regcomp => prevod do tvaru pro regexec
    fprintf(stderr, "Error pri kompilaci regexp!\n");
    return -1;
  }
    
  status = regexec(&re, msg.c_str(), 5, pmatch, 0);
  
  // nalezeni adresare a kodu zaslaneho klientem 
  if(status == 0){
    adr = msg.substr(pmatch[2].rm_so, pmatch[2].rm_eo-pmatch[2].rm_so);
    kod = atoi(msg.substr(pmatch[1].rm_so, pmatch[1].rm_eo-pmatch[1].rm_so).c_str());
  }
  else {
    msg = "8101 Nedodrzeni protokolu pro komunikaci!\n\r\n";
    *message = msg;
    return 1;
  }
  regfree( &re);
  
  switch(kod){
    case 7000: break;
    default: msg = "8102 Zaslani chybneho kodu - nedodrzeni protokolu pro komunikaci!\n\r\n"; *message = msg; return 1;
  }
  
  *adresar = adr;
  *message = msg;
  return 0; 
}
