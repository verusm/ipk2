/*                                                                            
*   Nazev: server.h                                                    
*   Autor: Vera Mullerova, xmulle17@stud.fit.vutbr.cz                                                                 
*   Datum: 12.4.2011                                                          
*   Popis: Hlavickovy souboru pro server.c.             
*/

#ifndef _SERVER_H_
#define _SERVER_H_

#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <ctype.h>
#include <string.h>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netdb.h>
#include <regex.h>
#include <locale.h>
#include <signal.h>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include <iostream>

#define CPARAM 5

using namespace std;

/** Deklarace pouzivanych funkci. */
int Connection(int port);
int printDir(string adresar, string *message);
int parseMsg(string *adresar, string *message);
void ChangeAdr(string c, string ch, string *adr);

#endif
